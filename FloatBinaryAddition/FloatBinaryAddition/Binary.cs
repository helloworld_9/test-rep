using System;

class Binary
{
    /// <summary>
    /// To Add Two Binary Numbers
    /// </summary>
    /// <param name="number1"></param>
    /// <param name="number2"></param>
    /// <param name="flag"></param>
    /// <returns></returns>
    public string Addition(string number1, string number2, int flag)
    {
        Utilities util = new Utilities();
        Binary bin = new Binary();
        int carry = 0;
        string fractional = String.Empty;
        string whole = String.Empty;
        string result = String.Empty;
        int decimalPoint = number1.IndexOf(".");
        int length = number1.Length;

        //To Perform Addition on Fractional Part
        for(int i=length-1; i>decimalPoint; i--)
        {
            int sum = carry + (number1[i] - '0') + (number2[i] - '0');
            if(sum == 1)
            {
                fractional += '1';
                if(carry == 1)
                {
                    carry = 0;
                }
            }
            else if(sum == 2)
            {
                fractional += '0';
                carry = 1;
            }
            else if(sum == 3)
            {
                fractional += '1';
                carry = 1;
            }
            else
            {
                fractional += '0';
            }
            
        }
        fractional = util.StringReverse(fractional);
        
        //To Perform Addition on Whole Part
        for(int i=decimalPoint-1;i>=0;i--)
        {
            int sum = carry + (number1[i] - '0') + (number2[i] - '0');
            if(sum == 1)
            {
                whole += '1';
                if(carry == 1)
                {
                    carry = 0;
                }
            }
            else if(sum == 2)
            {
                whole += '0' ;
                carry = 1;
            }
            else if(sum == 3)
            {
                whole += '1';
                carry = 1;
            }
            else
            {
                whole += '0';
            }
        }
        whole = util.StringReverse(whole);
        
        //To Compute Result Based on Signed Bit
        if(flag == 0)
        {
            if(carry != 0)
            {
                result += carry;
            }
            result += whole;
            result += ".";
            result += fractional;
        }
        else
        {
            result += whole;
            result += ".";
            result += fractional;
            if(result[0] == '1')
            {
                result = bin.TwosCompliment(result);
                Console.Write("-");
            }
        }
        
        return result;
    }

    /// <summary>
    /// To Perform Two's Compliment of Binary Number
    /// </summary>
    /// <param name="number"></param>
    /// <returns></returns>
    public string TwosCompliment(string number)
    {
        int length = number.Length;
        string tempNumber = String.Empty;
        int lastIndex = number.LastIndexOf("1");
        if(lastIndex != -1)
        {
            for(int i=0; i<lastIndex; i++)
            {
                if(number[i] == '0')
                {
                    tempNumber += '1';
                }
                else if(number[i] == '1')
                {
                    tempNumber += '0';
                }
                else
                {
                    tempNumber += '.';
                }
            }
            for(int i=lastIndex; i<length; i++)
            {
                tempNumber += number[i];
            }
        }
        return tempNumber;
    }
}