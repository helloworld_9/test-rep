using System;

class Conversion
{
    /// <summary>
    /// To Convert Binary to Decimal
    /// </summary>
    /// <param name="number"></param>
    /// <returns></returns>
    public double ToDecimal(string number)
    {
        Utilities util = new Utilities();
        double whole = 0;
        double fractional = 0.0;
        int length = number.Length;
        int decimalPosition = number.IndexOf(".");

        //To Convert Binary Whole Part to Decimal Whole Part
        for(int i=0; i<decimalPosition; i++)
        {
            whole += (number[i] - '0')*util.Power(2,(decimalPosition-(i+1)));
        }

        //To Convert Binary Fractional Part to Decimal Fractional Part
        for(int i=decimalPosition+1; i<length; i++)
        {
            fractional += (number[i] - '0')*util.Power(2, (8-i));
        }

        double result = whole + fractional;
        return result;
    }

    /// <summary>
    /// To Convert Decimal to Binary
    /// </summary>
    /// <param name="number"></param>
    /// <returns></returns>
    public string ToBinary(string number)
    {
        Utilities util = new Utilities();
        if(number.Contains(".") == false)
        {
            number += ".0";
        }
        string tempNumber = String.Empty;
        int whole = 0;
        int binaryWhole = 0;
        double fractional = 0.0;
        int iterator = 0;
        int length = number.Length;

        //To Seperate Whole Part from the Given Float
        while(number[iterator]!='.')
        {
            whole += number[iterator] - '0';
            if(number[iterator+1]!='.')
            {
                whole *= 10;
            }
            iterator++;
        }
        iterator++;
        int divider = 10;

        //To Seperate Fractional Part from the Given Float
        while(iterator<length)
        {
            fractional += ((number[iterator] - '0')*1.0)/divider;
            divider *= 10;
            iterator++;
        }
        int remainder;
        int i = 1;

        //To Convert Decimal Whole Part to Binary Whole Part
        while(whole!=0)
        {
            remainder = whole % 2;
            whole /= 2;
            binaryWhole += remainder * i;
            i *= 10;
        }
        tempNumber += binaryWhole.ToString();
        tempNumber += ".";
        i = 0;

        //To Convert Decimal Fractional Part to Binary Fractional Part
        while(i<8)
        {
            fractional *= 2;
            int bit = Convert.ToInt32(util.Flooring(fractional));
            tempNumber += bit;
            if(bit == 1)
            {
                fractional = fractional - bit;
            }
            i++;
        }
        
        return tempNumber;
    }
}
