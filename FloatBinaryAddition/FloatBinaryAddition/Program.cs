﻿using System;

class Program
{
    /// <summary>
    /// Entry point of the program
    /// </summary>
    static void Main()
    {
        Conversion conversion = new Conversion();
        Binary binary = new Binary();
        Utilities util = new Utilities();
        string number1 = Console.ReadLine();
        string number2 = Console.ReadLine();
        string result = String.Empty;
        double float_result = 0.0;
        string tempNumber1 = String.Empty;
        string tempNumber2 = String.Empty;
        int number1Minus = 0;
        int number2Minus = 0;

        //To Set Sign Bit of the Given Number
        if(number1[0] == '-')
        {
            number1Minus = 1;
            number1 = number1.Substring(1,number1.Length-1);
        }
        if(number2[0] == '-')
        {
            number2Minus = 1;
            number2 = number2.Substring(1,number2.Length-1);
        }
        number1 = conversion.ToBinary(number1);
        number2 = conversion.ToBinary(number2);

        int number1Decimal = number1.IndexOf(".");
        int number2Decimal = number2.IndexOf(".");
        
        //To Pad Numbers Based on Their Lengths
        if(number1Decimal>number2Decimal)
        {
            tempNumber2 = util.Padding(number2, number1Decimal-number2Decimal);
            tempNumber1 = number1;
        }
        else if(number2Decimal>number1Decimal)
        {
            tempNumber1 = util.Padding(number1, number2Decimal-number1Decimal);
            tempNumber2 = number2;
        }
        else
        {
            tempNumber1 = number1;
            tempNumber2 = number2;
        }
        

        //To Perform Two's Compliment for Signed Binary Numbers
        if(number1Minus == 1 && number2Minus == 0)
        {
            tempNumber1 = binary.TwosCompliment(tempNumber1);
        }
        if(number2Minus == 1 && number1Minus == 0)
        {
            tempNumber2 = binary.TwosCompliment(tempNumber2);
        }

        //To Add Sign Bit to the First Binary Number
        if(number1Minus == 0)
        {
            number1 = "0";
            number1 += tempNumber1;
        }
        else
        {
            number1 = "1";
            number1 += tempNumber1;
        }
        
        //To Add Sign Bit to the Second Binary Number
        if(number2Minus == 0)
        {
            number2 = "0";
            number2 += tempNumber2;
        }
        else
        {
            number2 = "1";
            number2 += tempNumber2;
        }

        //To Perform Binary Addition Based on the Sign Bit
        if((number1Minus == 0 && number2Minus == 0) || (number1Minus == 1 && number2Minus == 1))
        {
            if(number1Minus == 1 && number2Minus == 1)
            {
                Console.Write("-");
            }
            number1 = '0' + number1.Substring(1);
            number2 = '0' + number2.Substring(1);
            result = binary.Addition(number1, number2, 0);
        }
        else
        {
            result = binary.Addition(number1, number2, 1);
        }
        
        float_result = conversion.ToDecimal(result);

        Console.WriteLine(float_result);
        Console.ReadLine();
    }
}