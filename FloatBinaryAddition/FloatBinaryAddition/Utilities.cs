using System;

class Utilities
{
    /// <summary>
    /// To Reverse a String
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    public string StringReverse(string str)
    {
        if (str.Length > 0)
        {
            return str[str.Length - 1] + StringReverse(str.Substring(0, str.Length - 1));
        }
        else
        {
            return str;
        }
    }

    /// <summary>
    /// To Perform Power Operation
    /// </summary>
    /// <param name="number_1"></param>
    /// <param name="number_2"></param>
    /// <returns></returns>
    public double Power(int number_1, int number_2)
    {
        double result = 1.0;
        int tempNumber = number_2;
        if(tempNumber < 0)
        {
            tempNumber *= -1;
        }
        while(tempNumber > 0)
        {
            result *= number_1;
            tempNumber--;
        }
        if(number_2<0)
        {
            result = 1.0 / result;
        }
        return result;
    }

    /// <summary>
    /// To Perform Floor Operation
    /// </summary>
    /// <param name="number"></param>
    /// <returns></returns>
    public double Flooring(double number)
    {
        double result = number - (number % 1);
        return result;
    }

    /// <summary>
    /// To Perform Padding of Bits
    /// </summary>
    /// <param name="number"></param>
    /// <returns></returns>
    public string Padding(string number, int pad)
    {
        string tempNumber = String.Empty;
        
        for(int iterator=0; iterator<pad; iterator++)
        {
            tempNumber += '0';
        }
        tempNumber += number;

        return tempNumber;
    }
}